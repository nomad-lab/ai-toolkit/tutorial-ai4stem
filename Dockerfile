FROM jupyter/tensorflow-notebook:python-3.9


# ================================================================================
# Linux applications and libraries
# ================================================================================

# USER root
# RUN apt update --yes \
#  && apt install --yes --quiet --no-install-recommends \
#     git \
#  && apt clean \
#  && rm -rf /var/lib/apt/lists/*


# ================================================================================
# Python environment
# ================================================================================
# TODO: build fixed requirement.txt

WORKDIR /tmp/

# COPY --chown=${NB_UID}:${NB_GID} ai4stem/ ./ai4stem
# RUN pip install --no-cache-dir --requirement ./ai4stem/requirements.txt \
#  && pip install --no-cache-dir ./ai4stem \
#  && fix-permissions "${CONDA_DIR}" \
#  && fix-permissions "/home/${NB_USER}"

RUN pip install --no-cache-dir 'git+https://github.com/AndreasLeitherer/ai4stem.git@v0.1.0' \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"

# ================================================================================
# Switch back to jovyan to avoid accidental container runs as root
# ================================================================================

WORKDIR ${HOME}
USER ${NB_UID}
ENV DOCKER_STACKS_JUPYTER_CMD="nbclassic"

# COPY --chown=${NB_UID}:${NB_GID} ai4stem.ipynb .
COPY --chown=${NB_UID}:${NB_GID} notebook/ ./
